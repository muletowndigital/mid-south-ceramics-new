<?php
/***********************************************************************************/
/* Define Constants */
/***********************************************************************************/

define('THEMEROOT', get_stylesheet_directory_uri());
define('SCRIPTS', get_stylesheet_directory_uri()) . "/js";
define('IMAGES', THEMEROOT . '/images');
define('FUNC_PATH', get_template_directory() . '/functions');
define('SVGPATH', $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/midsouthceramics-custom/images');
$use_typekit = false;

/***********************************************************************************/
/* Menus */
/***********************************************************************************/

function register_my_menus() {
	register_nav_menus(array(
		'main-menu' => __('Main Menu', 'sb'),
		'mobile-menu' => __('Mobile Menu', 'sb'),
      'mobile-menu-upper' => __('Mobile Menu Upper', 'sb'),
		'footer-menu-first' => __('Footer Menu First', 'sb'),
		'footer-menu-second' => __('Footer Menu Second', 'sb'),
		'footer-menu-third' => __('Footer Menu Third', 'sb'),
		'utility-menu' => __('Utility Menu', 'sb'),
	));
}
add_action('init', 'register_my_menus');

/***********************************************************************************/
/* SEO Press Function */
/***********************************************************************************/

function sp_content_analysis_content($content, $id) {
	//$content = default WP editor
	//$id = current post ID
	//Example to add your custom field to content analysis
	$cf = get_post_meta($id, 'my-custom-field', true);
   $cf2 = get_post_meta($id, 'my-custom-field2', true);
	$content = $content.$cf.$cf2;
	return $content;
}
add_filter('seopress_content_analysis_content', 'sp_content_analysis_content', 10, 2);

/***********************************************************************************/
/* Theme Additions */
/***********************************************************************************/

add_theme_support( 'post-thumbnails' );
add_theme_support( 'title-tag' );
add_image_size( 'post-thumb', 414, 270, true );

// Allow SVG Uploads

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

add_editor_style( THEMEROOT . '/css/partials/editor-style.css' );

// First, create a function that includes the path to your favicon
function add_favicon() {
  	$favicon_url = '/favicon.ico';
	echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
}

// Now, just make sure that function runs when you're on the login page and admin pages
add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');

// Font Styles in the Admin

add_filter( 'mce_buttons_2', 'fb_mce_editor_buttons' );
function fb_mce_editor_buttons( $buttons ) {

    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}

add_filter( 'tiny_mce_before_init', 'fb_mce_before_init' );

function fb_mce_before_init( $settings ) {

	$style_formats = array(

		// Font Colors for All Items

		array(
      	'title' => 'White',
         'selector' => 'p,h1,h2,h3,h4,h5,h6,table,ul,ol',
         'classes' => 'text--white',
     	),

     	array(
      	'title' => 'Black',
         'selector' => 'p,h1,h2,h3,h4,h5,h6,table,ul,ol',
         'classes' => 'text--black',
     	),

     	array(
      	'title' => 'Primary',
         'selector' => 'p,h1,h2,h3,h4,h5,h6,table,ul,ol',
         'classes' => 'text--primary',
     	),

     	array(
      	'title' => 'Secondary',
         'selector' => 'p,h1,h2,h3,h4,h5,h6,table,ul,ol',
         'classes' => 'text--secondary',
     	),

     	array(
      	'title' => 'Tertiary',
         'selector' => 'p,h1,h2,h3,h4,h5,h6,table,ul,ol',
         'classes' => 'text--tertiary',
     	),

      // SEO Header Replacements

      array(
        'title' => 'H1 Header Style',
         'selector' => 'p,h1,h2,h3,h4,h5,h6',
         'classes' => 'header--h1',
      ),

      array(
        'title' => 'H2 Header Style',
         'selector' => 'p,h1,h2,h3,h4,h5,h6',
         'classes' => 'header--h2',
      ),

      array(
        'title' => 'H3 Header Style',
         'selector' => 'p,h1,h2,h3,h4,h5,h6',
         'classes' => 'header--h3',
      ),

      array(
        'title' => 'H4 Header Style',
         'selector' => 'p,h1,h2,h3,h4,h5,h6',
         'classes' => 'header--h4',
      ),

      array(
        'title' => 'H5 Header Style',
         'selector' => 'p,h1,h2,h3,h4,h5,h6',
         'classes' => 'header--h5',
      ),

      array(
        'title' => 'H6 Header Style',
         'selector' => 'p,h1,h2,h3,h4,h5,h6',
         'classes' => 'header--h6',
      ),

        array(
            'title' => 'Header Looks Like Paragraph',
            'selector' => 'p,h1,h2,h3,h4,h5,h6',
            'classes' => 'header--p',
        ),

     	// Font Weights

     	array(
      	'title' => 'Normal Weight',
         'selector' => 'p,h1,h2,h3,h4,h5,h6',
         'classes' => 'text--weight-400',
     	),

     	array(
      	'title' => 'Bold Weight',
         'selector' => 'p,h1,h2,h3,h4,h5,h6',
         'classes' => 'text--weight-700',
     	),

     	array(
      	'title' => 'Light Weight',
         'selector' => 'p,h1,h2,h3,h4,h5,h6',
         'classes' => 'text--weight-300',
     	),
	);

   $settings['style_formats'] = json_encode( $style_formats );

   return $settings;

}

/***********************************************************************************/
/* Over-Rides */
/***********************************************************************************/

function new_excerpt_more($more) {
   global $post;
	return '...<p><a class="moretag button button3" href="'. get_permalink($post->ID) . '">Read More</a></p>';
}
add_filter('excerpt_more', 'new_excerpt_more');

function custom_excerpt_length( $length ) {
	return 35;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function chromefix_inline_css()
{
	wp_add_inline_style( 'wp-admin', '#adminmenu { transform: translateZ(0); }' );
}
add_action('admin_enqueue_scripts', 'chromefix_inline_css');

/***********************************************************************************/
/* Register Sidebars */
/***********************************************************************************/

if(function_exists('register_sidebar')) {

	// Blog Sidebar
	register_sidebar(array(
		'name' => __('Blog Sidebar', 'sb'),
		'id' => 'blog-sidebar',
		'description' => __('Custom sidebar built for the blog', 'sb'),
		'before_widget' => '<div class="sidebar-widget">',
		'after_widget' => '</div> <!-- end sidebar widget -->',
		'before_title' => '<h5>',
		'after_title' => '</h5>'
	));

}

/***********************************************************************************/
/* Custom CSS and Google Fonts */
/***********************************************************************************/

function silver_styles() {
	global $wp_styles;
  wp_enqueue_style( 'style', get_stylesheet_uri(), array(), '', 'all' );
	wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;500;600;700;800&display=swap');
}

add_action('wp_enqueue_scripts', 'silver_styles', 99);

/***********************************************************************************/
/* Typekit  */
/***********************************************************************************/

if($use_typekit === true) {

	function theme_typekit() {
		$typekit_id = 'zdn4qzz';
	   wp_enqueue_script( 'theme_typekit', '//use.typekit.net/' . $typekit_id . '.js');
	}
	add_action( 'wp_enqueue_scripts', 'theme_typekit' );

	function theme_typekit_inline() {
	  if ( wp_script_is( 'theme_typekit', 'done' ) ) { ?>
	  	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	<?php }
	}
	add_action( 'wp_head', 'theme_typekit_inline' );

	add_action( 'admin_head' , 'add_typekit_admin' );
	function add_typekit_admin() {
		$typekit_id = 'zdn4qzz';
	?>
	  <script type="text/javascript" src="//use.typekit.net/<?= $typekit_id; ?>.js"></script>
	  <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	<?php }

}

/***********************************************************************************/
/* Enqueue Scripts */
/***********************************************************************************/

function silverbox_scripts_with_jquery()
{


   // Register the library again from Google's CDN
	wp_register_script( 'scripts', get_template_directory_uri() . '/js/min/custom-scripts-min.js', array( 'jquery' ), null, true);
  wp_register_script( 'wow-call', get_template_directory_uri() . '/js/min/wow-call-min.js', array( 'jquery' ), null, true);
  //wp_register_script( 'general', get_template_directory_uri() . '/js/min/general-min.js', array( 'jquery' ), null, true);
  wp_register_script( 'general', get_template_directory_uri() . '/js/general.js', array( 'jquery' ), null, true);

  // For either a plugin or a theme, you can then enqueue the script:
  wp_enqueue_script( 'jquery' );
  wp_enqueue_script( 'scripts' );
  wp_enqueue_script( 'wow-call' );
  wp_enqueue_script( 'general' );

}

add_action( 'wp_enqueue_scripts', 'silverbox_scripts_with_jquery' );


/***********************************************************************************/
/* Muletown Admin Image */
/***********************************************************************************/

/*function change_my_wp_login_image() {
echo "
<style>
body.login #login h1 a {
background: url('https://muletowndigital.com/company/muletown-digital-logo-black.png') 0 0 no-repeat transparent;
height:244px;
width:325px;
position: relative;
left: 0px;
 }
</style>
";
}
add_action("login_head", "change_my_wp_login_image");

function my_login_logo_url() {
    return 'https://muletowndigital.com';
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Muletown Digital';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );*/


/***********************************************************************************/
/* TinyMCE Custom Buttons */
/***********************************************************************************/

// Recent Blog Posts

function silverbox_register_buttons( $buttons ) {
   array_push( $buttons, "|", "custom_buttons", "text_buttons", "customvideo", "fontawesome" );
   return $buttons;
}

function silverbox_add_plugin( $plugin_array ) {
   $plugin_array['silverbox'] = get_template_directory_uri() . '/mce-widgets/custom-buttons.js';
   return $plugin_array;
}

function silverbox_buttons() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'silverbox_add_plugin', 999 );
      add_filter( 'mce_buttons_2', 'silverbox_register_buttons' );
   }

}

add_action('init', 'silverbox_buttons');

/***********************************************************************************/
/* ACF Options Panel Setup */
/***********************************************************************************/

if( function_exists('acf_add_options_page') ) {

  acf_add_options_page(array(
    'page_title'  => 'Express Settings',
    'menu_title'  => 'Express Settings',
    'menu_slug'   => 'mtd-express-settings',
    'capability'  => 'edit_posts',
    'redirect'    => false,
    'icon_url' => IMAGES . '/mtd-options.svg',
    'position'    => 2,
  ));

  /*acf_add_options_sub_page(array(
    'page_title'  => 'Express General Settings',
    'menu_title'  => 'General',
    'parent_slug' => 'mtd-express-settings',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Express Social Media',
    'menu_title'  => 'Social Media',
    'parent_slug' => 'mtd-express-settings',
  ));*/

}

/**
 * Add WooCommerce support
 */
function theme_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'theme_add_woocommerce_support' );
/***********************************************************************************/
/* Included Functions */
/***********************************************************************************/

require_once(FUNC_PATH . "/shortcodes.php");
require_once(FUNC_PATH . "/mtd-elementor-widgets.php");
// WooCommerce functionality
require_once(FUNC_PATH . "/woo-custom.php");
require_once(FUNC_PATH . "/nav-menu.php");

/* Filter to change RCP Register button text to Signup */
/***********************************************************************************/


function tclw_rcp_registration_register_button( $button_text ) {
    return __( 'Signup', 'rcp' );
}

add_filter( 'rcp_registration_register_button', 'tclw_rcp_registration_register_button' );
