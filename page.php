<?php

	/* Default Page Template */

	get_header();

	// The Page ID
	$page_id = get_the_ID();
?>

<main class="internal-page wp-content">

	<!-- Begin Editor Page Content -->

	<section class="page-content">



			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

			<!-- WP Content -->

			<article class="wp-content" id="page-<?php the_ID(); ?>-content">

				<div class="editor-content">

				<?php the_content(); ?>

				</div>

			</article>

			<?php if(is_page(9)) { ?>
        <div class="container">
			<?php

				echo '<h4>Social Icons</h4>';

				// Style Guide grab ACF Elements

				if( have_rows('social_media_icons', 'option') ):

					echo '<ul class="social-icons">';

						while( have_rows('social_media_icons', 'option') ): the_row();
							echo '<li><a href="' . get_sub_field('social_link') . '" target="_blank">' . get_sub_field('social_icon') . '</a></li>';
						endwhile;

					echo '</ul>';

				endif;

			?>

			<?php
				if ( have_rows( 'featured_posts_module', 'option' ) ) :
					$fp_array = array();
					while ( have_rows( 'featured_posts_module', 'option' ) ) : the_row();
						$choose_your_featured_posts = get_sub_field( 'choose_your_featured_posts' );
						array_push($fp_array, $choose_your_featured_posts);
					endwhile;
				endif;
			?>

			<section class="featured-posts">

				<div class="container">

					<header class="featured-posts__header">
						<?php if ( have_rows( 'blog_module', 'option' ) ) : ?>
							<?php while ( have_rows( 'blog_module', 'option' ) ) : the_row(); ?>
								<?php echo '<h2>' . get_sub_field( 'blog_module_header' ) . '</h2>'; ?>
							<?php endwhile; ?>
						<?php endif; ?>

						<a href="<?= get_page_link($blog_page_id); ?>" class="btn btn--blue-to-dark">View All Articles</a>
					</header>

					<div class="featured-posts__row">

						<?php

							$args = array(
								'post_type' => 'post',
								'posts_per_page' => 3,
								'post__in' => $fp_array[0],
								'orderby' => 'post__in'
							);

							$loop = new WP_Query($args);

							if($loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post();
								get_template_part('loops/posts', get_post_format());
							endwhile; endif; wp_reset_postdata();

						?>

					</div>

					<div class="featured-posts__button-mobile">
						<a href="<?= get_page_link($blog_page_id); ?>" class="btn btn--blue-to-dark">View All Articles</a>
					</div>

				</div>

			</section>


			<?php } ?>

			<?php endwhile; endif; ?>

		</div>

	</section>

</main>

<?php

	get_footer();

?>
