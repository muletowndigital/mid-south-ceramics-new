<?php

// Global Variables

$site_name = get_bloginfo('name');
$site_author = "Muletown Digital";
$settings = get_option('theme_options');

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>

    <!-- Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="author" content="<?= $site_author; ?>">

    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="57x57" href="/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicons/ms-icon-144x144.png">

    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?> id="post-<?php echo get_the_ID(); ?>">
<?php do_action( 'wp_body_open' ); ?>

<div class="full-wrapper">

    <!-- Header Main -->

    <header class="masthead">

        <div class="container">

            <div class="masthead__flex">

                <div class="masthead__logo">
                    <a href="/">
                        <?php
                        $header_logo = get_field('company_logo_color', 'option');
                        if (!empty($header_logo)) {
                            echo '<img src="' . $header_logo['url'] . '" alt="' . $header_logo['alt'] . '" />';
                        } else {
                            echo $site_name;
                        }
                        ?>
                    </a>
                </div>


                <div class="masthead__nav">
                    <nav class="masthead__utility-nav main-navigation">
                        <div class="masthead__utility-nav-container">
                            <?php
                            wp_nav_menu(array(
                                'theme_location' => 'utility-menu'
                            ));
                            ?>
                            <div class="search masthead__search">
                                <button class="masthead__btn js-search"><?php _e('Search', 'midsouthceramics'); ?><i
                                            class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </nav>
                    <nav class="masthead__main-nav main-navigation">
                        <?php
                        wp_nav_menu(array(
                            'theme_location' => 'main-menu',
                            'walker' => new MTD_Enhanced_Walker()
                        ));
                        ?>

                        <div class="masthead__mini-cart">
                            <?php global $woocommerce; ?>
                            <a data-count="<?php echo $woocommerce->cart->cart_contents_count; ?>" class="cart-button"
                               href="#">
                            </a>
                            <a href="<?php echo wc_get_cart_url(); ?>" class="masthead__btn js-mini-cart"><i
                                        class="fas fa-shopping-cart"> <span class="cart-count"></span>
                                </i><?php _e('Cart', 'midsouthceramics'); ?></a>
                        </div>

                        <?php $header_btn = get_field('header_button', 'options'); ?>
                        <?php if ($header_btn):
                            $link_url = $header_btn['url'];
                            $link_title = $header_btn['title'];
                            $link_target = $header_btn['target'] ? $header_btn['target'] : '_self'; ?>
                            <a class="btn btn--one btn--norm masthead__button"
                               href="<?php echo esc_url($link_url); ?>"
                               target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        <?php endif; ?>

                    </nav>
                </div>

                <!-- Mobile Buttons -->

                <div class="masthead__nav-wrap">

                    <div class="masthead__mobile-buttons">

                        <div class="search masthead__search">
                            <button class="masthead__btn js-search"><i
                                        class="fas fa-search"></i></button>
                        </div>

                        <div class="masthead__mini-cart">
                            <?php global $woocommerce; ?>
                            <a data-count="<?php echo $woocommerce->cart->cart_contents_count; ?>" class="cart-button"
                               href="#">
                            </a>
                            <a href="<?php echo wc_get_cart_url(); ?>" class="masthead__btn js-mini-cart"><i
                                        class="fas fa-shopping-cart"> <span class="cart-count"></span>
                                </i></a>
                        </div>

                    </div>

                    <!-- Mobile Triggers -->

                    <div class="nav-triggers">
                        <a class="hamburger js-mm-trigger" type="button" role="button" aria-label="Toggle Navigation">
                            <span class="line1"></span>
                            <span class="line2"></span>
                            <span class="line3"></span>
                        </a>
                    </div>

                </div>

            </div>

        </div>

    </header>


    <header class="masthead masthead--fixed"></header>