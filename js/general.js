jQuery(document).ready(function($) {

	$(window).scroll();

	// Header Clone

	$('.masthead .container').clone().prependTo('.masthead--fixed');

	// Mobile Menu Trigger

	$('.js-mm-trigger').on('click', function(e) {
		$(this).toggleClass('js-mm-close');
		$('.mm').toggleClass('mm--show');
		$('body').toggleClass('locked');
		e.preventDefault();
	});

	// My Custom JS

	const searchToggle = $('.js-search');
	const searchHeader = $('.masthead__searchform');
	const bodyOverlay = $('.masthead__overlay');
	searchToggle.on('click', function () {
		searchHeader.toggleClass('active');
		bodyOverlay.toggleClass('active');
	});


	//Shop page
	$(".archive-title .elementor-heading-title").text($(".archive-title .elementor-heading-title").text().replace("Category:", "Shop"));

	//Search Form
	$('.masthead__close').click(function () {
		searchHeader.removeClass('active');
		bodyOverlay.removeClass('active');
	});

	//Product Slider Button
	let productItem = $('.elementor-widget-eael-woo-product-carousel .product');
	productItem.each( function (){
		let productLink = $(this).find('.image-wrap a').attr('href');
		let productDetailContainer = $(this).find('.product-details-wrap');
		productDetailContainer.append('<a href="" class="broduct-custom-btn btn btn--two btn--norm">SELECT OPTIONS</a>');
		productDetailContainer.find('.broduct-custom-btn').attr("href", productLink);
	});

	//Submenu two columns
	let headeSubMenu = $('.masthead .menu .sub-menu');
	headeSubMenu.each(function(){
		if($(this).children().length > 8) {
			$(this).addClass('two-col');
		}
	});

	//Mobile Submenu Menu Buttons
	var menuParent = $('.mm .menu-item-has-children');
	var button = '<button class="submenu-toggle"><i class="far fa-chevron-down"></i></button>';
	menuParent.append(button);
	$(".submenu-toggle").click(function () {
		$this = $(this);
		$this.siblings('.sub-menu').slideToggle();
		$this.toggleClass('open');
	});

	var footerButton = $('.js-footer-btn');
	footerButton.click(function () {
		$this = $(this);
		$this.siblings('.footer__naw-wrap-mobile').slideToggle();
	});

	var productCats= $('.product-cats');
	var catBtn = productCats.find('h5')
	var productCategories = productCats.find('.product-categories')
	if ($(window).width() < 768) {
		catBtn.on('click', function(){
			productCategories.slideToggle();
		})
	}

	//Reset button
	var resetVariations = $('.reset_variations');
	var xButton = '<i class="fal fa-times"></i>';
	resetVariations.prepend(xButton);

	//Shop page append ID to the href for scrolling to the product section
	$('.product-categories a').click(function(e)
	{
		e.preventDefault();
		window.location = $(this).attr('href') + '#shop-list';
	});

	/***********************************************************************************/
	/* Local Scrolling */
	/***********************************************************************************/

	$('a[href*="#"]')
	  // Remove links that don't actually link to anything
	  .not('[href="#"]')
	  .not('[href="#0"]')
	  .click(function(event) {
	    // On-page links
	    if (
	      location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
	      // Figure out element to scroll to
	      var target = $(this.hash);
	      var target_offset = 0;

	      if($(window).width() > 1040) {
	      	target_offset = 0;
	      }

	      else {
	      	target_offset = $('.masthead').outerHeight();
	      }

	      target = target.length ? target : $('.' + this.hash.slice(1));
	      // Does a scroll target exist?
	      if (target.length) {
	        // Only prevent default if animation is actually gonna happen
	        event.preventDefault();
	        $('html, body').animate({
	          scrollTop: target.offset().top - target_offset
	        }, 1000, function() {
	          // Callback after animation
	          // Must change focus!
	          var $target = $(target);

	          if ($target.is(":focus")) { // Checking if the target was focused
	            return false;
	          } else {
	            //$target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
	           // $target.focus(); // Set focus again
	          }
	        });
	      }
	   }
	});


	/***********************************************************************************/
	/* Resize Functions */
	/***********************************************************************************/

	// Window Resize Functions

	$(window).resize(function(){

		var window_width = $(window).width();

		if(window_width > 835) {
			$('.js-mm-trigger').removeClass('js-mm-close');
			$('.mm').removeClass('mm--show');
			$('body').removeClass('locked');
		}

	});

	$(window).resize();

	/***********************************************************************************/
	/* Scrolling Functions */
	/***********************************************************************************/

	var lastScrollTop = 0;

	$(window).scroll(function () {
		var top_pos = $(window).scrollTop();
		//var window_width = $(window).width();

		if (top_pos < lastScrollTop && top_pos > 200){
	      // upscroll code
	   	$('.masthead--fixed').addClass('masthead--fixed--on');
	   	$('body').addClass('header-lock');
	   }
		if ($(window).width() < 768 && top_pos > 200) {
			$('.masthead--fixed').addClass('masthead--fixed--on');
		}

	   else {
	      // upscroll code
	   	$('body').removeClass('header-lock');
	   	$('.masthead--fixed').removeClass('masthead--fixed--on');
	   }

		if(top_pos > 100) {
			$('.nav-triggers a').addClass('mm--scrolled');
		}

		else {
			$('.nav-triggers a').removeClass('mm--scrolled');
		}

  		lastScrollTop = top_pos;

	});

	$(window).scroll();

});
