<?php
	
	// BLOG - SINGLE TEMPLATE

	get_header();

	$blog_page = get_option( 'page_for_posts' );

	$post_id = get_the_ID();
	
?>

<main class="post-single">

	<div class="page-container wp-content">

		<!-- Page Banner / Feature Area -->

		<?php //require_once('include/parts/page-hero.php'); ?>

		<header class="single-post-header">

			<div class="container">
			
				<?php

					$cats = get_the_category();
					$tags = get_the_tags();

					if(!empty($cats)) {
						echo '<div class="single-post-header__cats">';
						$cats_array = array();
						$count = count($cats);
						$cycle = 1;
						foreach($cats as $cat) {
							array_push($cats_array, $cat->term_id);
							echo '<a href="' . get_category_link($cat->term_id) . '" class="cats">' . $cat->name . '</a>';
							if($cycle < $count) {
								echo ', ';
							}

							$cycle++;
						}
						echo '</div>';
					}
				?>

				<h1><?= the_title(); ?></h1>	

			</div>		

		</header>

		<section class="single-post-outer">

			<div class="container">

				<div class="single-post-outer__content">
											
					<article class="single-article" id="article-<?php the_ID(); ?>">
						
						<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

							<?php the_content(); ?>
					
						<?php endwhile; endif; ?>				
							
					</article>

					<!-- Related Posts -->

					<?php

						$rp_args = array(
							'post_type' => 'post',
							'cat' => $cats_array,
							'post__not_in' => array($post_id),
							'posts_per_page' => 2,
							'orderby' => 'menu_order',
						);

						$related_posts = new WP_Query($rp_args);

						if($related_posts->post_count < 1) {

							$rp_args = array(
								'post_type' => 'post',
								'post__not_in' => array($post_id),
								'posts_per_page' => 2,
							);

							$related_posts = new WP_Query($rp_args);
						}

						if($related_posts->have_posts() ) :
					?>

					<!-- Related Articles -->

					<div class="related-posts">

						<div class="container">

							<div class="related-posts__inner">
							
								<header class="related-posts__head">
									<h4 class="text--tertiary">Related Posts</h4>
								</header>

								<div class="blog-flex-row">

									<?php 

									while ( $related_posts->have_posts() ) : $related_posts->the_post(); 				
										get_template_part('loops/posts', get_post_format());
									endwhile;

									?>

								</div>

							</div>

						</div>

					</div>

					<?php endif; wp_reset_postdata(); ?>

				</div>

				<aside class="single-post-outer__sidebar sidebar blog-sidebar">
					<?php 
						if (is_active_sidebar('blog-sidebar')) :
							dynamic_sidebar('blog-sidebar');
						endif;
					?>
				</aside>

			</div>

		</section>

	</div>

</main>

<?php get_footer(); ?>