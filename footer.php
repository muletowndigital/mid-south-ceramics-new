<?php
$settings = get_option('theme_options');
$footer_logo = get_field('footer_logo', 'options');
$company_address = get_field('company_address', 'options');
$company_email_address = get_field('company_email_address', 'options');
$company_phone_number = get_field('company_phone_number', 'options');
$copyright_statement = get_field('copyright_statement', 'options');
$build_tagline = get_field('build_tagline', 'options');
$social_media_icons = get_field('social_media_icons', 'options');
$mobile_menu_button = get_field('mobile_menu_button', 'options');
$footer_button = get_field('footer_button', 'options');
?>

<!-- Footer -->

<footer class="footer">

    <div class="container">


        <div class="row menus-wrapper">

            <button class="btn footer__mobile-btn js-footer-btn"><?php _e('Full Menu +', 'midsouthceramics') ;?></button>

            <div class="footer__naw-wrap-mobile">
                <p class="footer__subheading"><?php _e('Company', 'midsouthceramics') ;?></p>
                <nav class="footer__nav-mobile">
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'footer-menu-first'
                    ));
                    ?>
                </nav>

                <p class="footer__subheading"><?php _e('Shop', 'midsouthceramics') ;?></p>
                <nav class="footer__nav-mobile">
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'footer-menu-second'
                    ));
                    ?>
                </nav>

                <nav class="footer__nav-mobile">
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'footer-menu-third'
                    ));
                    ?>
                </nav>
            </div>

        </div>
        <div class="row contacts-container">
            <?php if ($footer_logo) { ?>
                <div class="footer__logo">
                    <a href="/">
                        <?php echo wp_get_attachment_image($footer_logo['ID'], 'medium', false, array('class' => '')); ?>
                    </a>
                </div>
            <?php } ?>

            <?php if ($company_address || $company_phone_number || $company_email_address || $social_media_icons) { ?>
                <div class="footer__contacts">
                    <p class="footer__subheading"><?php _e('Contact Us', 'midsouthceramics') ;?></p>

                    <?php if ($company_address) { ?>
                        <div class="footer__address">
                            <?php echo $company_address; ?>
                        </div>
                    <?php } ?>

                    <?php if ($company_phone_number) { ?>
                        <a class="footer__phone" href="tel:<?php echo $company_phone_number; ?>"><?php echo $company_phone_number; ?></a>
                    <?php } ?>
                    <?php if ($company_email_address) { ?>
                        <a class="footer__mail"
                           href="mailto:<?php echo $company_email_address; ?>"><?php echo $company_email_address; ?></a>
                    <?php } ?>

                    <?php if (have_rows('social_media_icons', 'options')): ?>
                        <div class="footer__socials">
                            <?php while (have_rows('social_media_icons', 'options')) : the_row(); ?>

                                <?php $social_icon = get_sub_field('social_icon'); ?>
                                <?php $social_link = get_sub_field('social_link'); ?>
                                <a class="footer__social" target="_blank"
                                   href="<?php echo $social_link; ?>"><?php echo $social_icon; ?></a>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($footer_button):
                        $link_url = $footer_button['url'];
                        $link_title = $footer_button['title'];
                        $link_target = $footer_button['target'] ? $footer_button['target'] : '_self'; ?>
                        <a class="footer__button"
                           href="<?php echo esc_url($link_url); ?>"
                           target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                    <?php endif; ?>
                </div>


                <div class="footer__nav footer__nav-first">
                    <p class="footer__subheading"><?php _e('Company', 'midsouthceramics') ;?></p>
                    <nav class="footer__nav">
                        <?php
                        wp_nav_menu(array(
                            'theme_location' => 'footer-menu-first'
                        ));
                        ?>
                    </nav>
                </div>

                <div class="footer__nav footer__nav-second">
                    <p class="footer__subheading"><?php _e('Shop', 'midsouthceramics') ;?></p>
                    <nav class="footer__nav">
                        <?php
                        wp_nav_menu(array(
                            'theme_location' => 'footer-menu-second'
                        ));
                        ?>
                    </nav>
                </div>

                <div class="footer__nav footer__nav-third">
                    <nav class="footer__nav">
                        <?php
                        wp_nav_menu(array(
                            'theme_location' => 'footer-menu-third'
                        ));
                        ?>
                    </nav>
                </div>

            <?php } ?>
        </div>

        <?php if ($copyright_statement || $build_tagline) { ?>
            <div class="row">
                <div class="footer__copyright">
                    <?php echo $copyright_statement; ?>
                    <?php echo $build_tagline; ?>
                </div>
            </div>
        <?php } ?>
    </div>

</footer>

</div>

<section class="mm mobile-menu-container">

    <nav class="mm__menu mobile-menu">
        <?php
        wp_nav_menu(array(
            'theme_location' => 'mobile-menu-upper'
        ));
        ?>
    </nav>
    <nav class="mm__menu-utility mobile-menu">
        <?php
        wp_nav_menu(array(
            'theme_location' => 'mobile-menu'
        ));
        ?>
    </nav>

    <div class="mm__contact">
        <?php if (have_rows('social_media_icons', 'options')): ?>
            <div class="mm__socials">
                <?php while (have_rows('social_media_icons', 'options')) : the_row(); ?>
                    <?php $social_icon = get_sub_field('social_icon'); ?>
                    <?php $social_link = get_sub_field('social_link'); ?>
                    <a class="mm__social" target="_blank"
                       href="<?php echo $social_link; ?>"><?php echo $social_icon; ?></a>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>

        <?php if ($company_address) { ?>
            <div class="mm__address">
                <?php echo $company_address; ?>
            </div>
        <?php } ?>

        <?php if ($company_phone_number) { ?>
            <a class="mm__phone" href="tel:<?php echo $company_phone_number; ?>"><?php echo $company_phone_number; ?></a>
        <?php } ?>

    </div>

</section>

<div class="masthead__searchform">
    <button class="masthead__close"><i class="fas fa-times close-filters"></i></button>
    <h2><?php _e('Search Mid-South Ceramics', 'midsouthceramics'); ?></h2>
    <form method="get" id="searchform" action="<?php bloginfo('url'); ?>/">
        <div class="search-feilds-wrapper">
        <input type="text" value="<?php the_search_query(); ?>" name="s" id="s" m
               placeholder="Search"/>
               <div class="search-products-input">
            <input type="checkbox" id="search-product-checkbox" name="post_type" value="product">
            <label for="search-product-checkbox">Search Products Only</label>
            </div>
            </div>
        <input type="submit" id="searchsubmit" value="Search" class="button button3"/>
        
    </form>
</div>
<div class="masthead__overlay"></div>

<?php wp_footer(); ?>

</body>
</html>