<?php
$posts_link = get_post_type_archive_link('post');
$terms = get_terms('category', [
    'hide_empty' => true,
]);
$current_id = get_queried_object()->term_id;
$active_blog = $current_id == '' ? 'active-cat blog-cat' : 'blog-cat';
?>

<h4 class="blog-cat__heading"><?php _e('Categories', 'midsouthceramics'); ?></h4>
<div class="cat_wrapper">
    <a class="<?php echo $active_blog ;?>" href="<?php echo $posts_link; ?>"><?php _e('All News', 'midsouthceramics'); ?></a>
    <?php foreach ($terms as $key => $term) : ?>
    <?php $active_cat = $current_id === $term->term_id ? 'active-cat blog-cat' : 'blog-cat' ;?>
        <a class="<?php echo $active_cat ;?>" href="<?php echo get_term_link($term->term_id); ?>"><?php echo $term->name; ?></a>
    <?php endforeach; ?>
</div>
