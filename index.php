<?php
	
	// Main Blog Index Page
	
	get_header();

	$page_id = get_option( 'page_for_posts' );
		
?>

<!-- Blog / Page Content -->

<main class="blog-posts">

	<!-- Blog Wrapper -->

	<section class="blog-wrapper">
		
		<div class="container">
			
			<div class="blog-outer">
											
				<div class="blog-outer__content">
		
					<div class="row">
			
						<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
		
						<?php get_template_part('loops/posts', get_post_format()); ?>
					
						<?php endwhile; endif; ?>
		
					</div> <!-- /row -->

					<div class="wp-page-navi">
					
						<?php wp_pagenavi(); ?>

					</div>
						
				</div>

				<aside class="blog-outer__sidebar sidebar blog-sidebar">
				<?php 
					if (is_active_sidebar('blog-sidebar')) :
						dynamic_sidebar('blog-sidebar');
					endif;
				?>
				</aside>
				
			</div> <!-- /flex-container-->

		</div>
						
	</section>

</main>

<?php get_footer(); ?>