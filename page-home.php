<?php

/* Template Name: Homepage */

get_header();

// The Page ID
$page_id = get_the_ID();

if(have_posts()) : while(have_posts()) : the_post();

?>

<main class="homepage wp-content">

	<!-- Content -->

	<section class="main-content">


			<?= the_content(); ?>


	</section>

</main>

<?php endwhile; endif; ?>

<?php

	get_footer();

?>
