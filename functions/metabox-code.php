<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 * @link http://www.deluxeblogtips.com/meta-box/
 */


add_filter( 'rwmb_meta_boxes', 'express_register_meta_boxes' );

/**
 * Register meta boxes
 *
 * @return void
 */
function express_register_meta_boxes( $meta_boxes )
{
	/**
	 * Prefix of meta keys (optional)
	 * Use underscore (_) at the beginning to make keys hidden
	 * Alt.: You also can make prefix empty to disable it
	 */
	// Better has an underscore as last sign
	$prefix = 'express_';

	/***********************************************************************************/
	/* Homepage Welcome */
	/***********************************************************************************/

	$meta_boxes[] = array(
		'id' => '',
		'title' => __( 'Metabox Code Test', 'rwmb' ),
		'pages' => array( 'page' ),
		/*'include' => array(
	      // With all conditions below, use this logical operator to combine them. Default is 'OR'. Case insensitive. Optional.
	      'relation' => 'OR',
	      'template' => array( '' ),
	  	),*/
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => true,
	
		'fields' => array(
			// 
			array(
				'name' => __( 'Testing Code', 'rwmb' ),
				'id'   => "testing_on_code",
				'type' => 'wysiwyg',
				'raw'  => false,
				'std'  => __( '', 'rwmb' ),
				'options' => array(
					'textarea_rows' => 6,
					'teeny'         => false,
					'media_buttons' => false,
				),
			),
		),
	);
	

	// Quotes meta box

	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id' => 'quotes',

		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title' => __( 'Quote Options', 'rwmb' ),

		// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
		'pages' => array( 'quotes' ),

		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context' => 'normal',

		// Order of meta box: high (default), low. Optional.
		'priority' => 'high',

		// Auto save: true, false (default). Optional.
		'autosave' => true,

		// List of meta fields
		'fields' => array(

			array(
				// Field name - Will be used as label
				'name'  => __( 'Author Name', 'rwmb' ),
				// Field ID, i.e. the meta key
				'id'    => "quote_author_name",
				// Field description (optional)
				'desc'  => __( 'Chris Krueger', 'rwmb' ),
				'type'  => 'text',
				// Default value (optional)
				'std'   => __( '', 'rwmb' ),
				// CLONES: Add to make the field cloneable (i.e. have multiple value)
				'clone' => false,
			),

			array(
				// Field name - Will be used as label
				'name'  => __( 'Author Data', 'rwmb' ),
				// Field ID, i.e. the meta key
				'id'    => "quote_author_data",
				// Field description (optional)
				'desc'  => __( 'EX: Owner of Foop-a-Loop LLC.', 'rwmb' ),
				'type'  => 'text',
				// Default value (optional)
				'std'   => __( '', 'rwmb' ),
				// CLONES: Add to make the field cloneable (i.e. have multiple value)
				'clone' => false,
			),
		),
	);

	
	return $meta_boxes;
}

// Register settings page. In this case, it's a theme options page
add_filter( 'mb_settings_pages', 'prefix_options_page' );
function prefix_options_page( $settings_pages )
{
	$settings_pages[] = array(
		'id'          => 'theme_options',
		'option_name' => 'theme_options',
		'menu_title'  => __( 'Express Options', 'textdomain' ),
		'icon_url'    => IMAGES . '/mtd-options.svg',
		'style'       => 'no-boxes',
		'columns'     => 1,
		'tabs'        => array(
			'general' => __( 'General Settings', 'textdomain' ),
			'social' => __( 'Social Media', 'textdomain' ),
		),
		'position'    => 2,
	);
	return $settings_pages;
}

// Register meta boxes and fields for settings page
/*add_filter( 'rwmb_meta_boxes', 'prefix_options_meta_boxes' );
function prefix_options_meta_boxes( $meta_boxes )
{


	return $meta_boxes;
}*/
