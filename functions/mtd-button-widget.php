<?php
namespace Elementor;

class MTD_Custom_Button extends Widget_Base {

	public function get_name() {
		return 'mtd-button-widget';
	}

	public function get_title() {
		return 'Custom Button';
	}

	public function get_icon() {
		return 'fad fa-hand-pointer';
	}

	public function get_categories() {
		return [ 'muletown-digital-elementor' ];
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Content', 'elementor' ),
			]
		);

		$this->add_control(
			'button_text',
			[
				'label' => __( 'Button Text', 'elementor' ),
				'label_block' => true,
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your button text', 'elementor' ),
			]
		);

		$this->add_control(
			'button_style',
			[
				'label' => __( 'Button Style', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'btn--one',
				'options' => [
					'btn--one'  => __( 'Button White', 'plugin-domain' ),
					'btn--two'  => __( 'Button Grey', 'plugin-domain' ),
					'btn--three'  => __( 'Button Light Blue', 'plugin-domain' ),
				],
			]
		);

		$this->add_control(
			'button_size',
			[
				'label' => __( 'Button Size', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'btn--norm',
				'options' => [
					'btn--norm'  => __( 'Standard Size', 'plugin-domain' ),
					'btn--lg'  => __( 'Large Size', 'plugin-domain' ),
					'btn--sm'  => __( 'Small Size', 'plugin-domain' ),
				],
			]
		);

		$this->add_control(
			'button_align',
			[
				'label' => __( 'Alignment', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'plugin-domain' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'plugin-domain' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'plugin-domain' ),
						'icon' => 'fa fa-align-right',
					],
				],
				'default' => 'left',
				'toggle' => true,
			]
		);

		$this->add_control(
			'button_link',
			[
				'label' => __( 'Link', 'elementor' ),
				'type' => Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'elementor' ),
				'default' => [
					'url' => '',
					'is_external' => false,
					'nofollow' => false,
				]
			]
		);

		$this->end_controls_section();
	}

	protected function render() {

      $settings = $this->get_settings_for_display();

      // Button Settings
		$target = $settings['button_link']['is_external'] ? ' target="_blank"' : '';
		$nofollow = $settings['button_link']['nofollow'] ? ' rel="nofollow"' : '';
      $url = $settings['button_link']['url'];
		echo  '<div class="button-container button-container--' . $settings['button_align'] . '"><a href="' . $settings['button_link']['url'] . '" class="btn ' . $settings['button_style'] . ' ' . $settings['button_size'] . '"' . $target . $nofollow .'>' . $settings['button_text'] . '</a></div>';


	}

	protected function _content_template() {

   }


}
?>
