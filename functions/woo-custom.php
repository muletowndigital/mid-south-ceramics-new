<?php
/***********************************************************************************/
/* WOOCOMMERCE FUNCTIONS */
/***********************************************************************************/

//Header Cart Counter
add_filter('woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');
function woocommerce_header_add_to_cart_fragment($fragments)
{
    ob_start();
    ?>
    <span class="cart-count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
    <?php
    $fragments['span.cart-count'] = ob_get_clean();
    return $fragments;
}

// display an 'Out of Stock' label on archive pages
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_stock', 10 );
function woocommerce_template_loop_stock() {
    global $product;
    if ( ! $product->managing_stock() && ! $product->is_in_stock() )
        echo '<span class="outofstock-badge">Stock Out</span>';
}

add_action( 'after_setup_theme', 'product_gallery_settings' );

// Adding theme support Gallery: Zoom images/Lightbox/Slider
function product_gallery_settings() {
   add_theme_support( 'wc-product-gallery-zoom' );
   add_theme_support( 'wc-product-gallery-lightbox' );
   add_theme_support( 'wc-product-gallery-slider' );
}
