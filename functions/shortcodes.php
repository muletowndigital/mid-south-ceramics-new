<?php
/***********************************************************************************/
/* Shortcodes */
/***********************************************************************************/

// Wrappers
function wrapper_build($atts, $content = null) {
   extract(shortcode_atts(array('class' => 'div'), $atts));
   return '<div class="' . $class . '">' . do_shortcode($content) . '</div>';
}
add_shortcode('div', 'wrapper_build');

// Accordion Wrappers
function accordion($atts, $content = null) {
   return '<dl class="accordion">' . do_shortcode($content) . '</dl>';
}
add_shortcode('accordion', 'accordion');

// Accordion Questions
function question($atts, $content = null) {
   extract(shortcode_atts(array('title' => 'Enter Your Title Here'), $atts));
   return '<div class="block-faq"><dt><a href="" class="icon-faq"><i class="fa fa-plus-circle" aria-hidden="true"></i></a><h6 class="title-faq">' . do_shortcode($title) . '</h6></dt><dd>' . do_shortcode($content) . '</dd></div>';
}
add_shortcode('question', 'question');

// Buttons
function button($atts, $content = null) {
	 extract(shortcode_atts(array('title' => 'Button Needs Text', 'class' => "button1", 'link' => '#', 'align' => 'left', 'page_id' => '', 'target' => ''), $atts));
	 $button_title = $title;
	 $button_class = $class;

	 if($page_id == "") {
	 	$button_link = $link;
	 }
	 else {
	 	$button_link = get_page_link($page_id);
	 }

	return '<div class="button-container button-container--' . $align . '"><a href="' . $button_link . '" class="btn ' . $class . '" target="' . $target . '">' . $title . '</a></div>';
}
add_shortcode('button', 'button');

// Buttons
function text_button($atts, $content = null) {
   extract(shortcode_atts(array('title' => 'Button Needs Text', 'class' => "button1", 'link' => '#', 'align' => 'left', 'page_id' => '', 'target' => ''), $atts));
   $button_title = $title;
   $button_class = $class;

   if($page_id == "") {
    $button_link = $link;
   }
   else {
    $button_link = get_page_link($page_id);
   }

  return '<div class="button-container button-container--' . $align . '"><a href="' . $button_link . '" class="' . $class . '" target="' . $target . '">' . $title . '</a></div>';
}
add_shortcode('text_button', 'text_button');

// Font Awesome Icons
function fa($atts, $content = null) {
   extract(shortcode_atts(array('icon' => '', 'size' => '24', 'link' => '', 'target' => ''), $atts));

   if($link != "") {
  	 $fa_icon = '<a href="' . $link . '" target="' . $target . '"><span class="fa ' . $icon . ' fw" style="font-size: ' . $size . 'px;"><i>FA Icon</i></span></a>';
  	}
  	else {
  		 $fa_icon = '<span class="fa ' . $icon . ' fw" style="font-size: ' . $size . 'px;"><i>FA Icon</i></span>';
  	}

  	return $fa_icon;
}
add_shortcode('fa', 'fa');

// Youtube Embed
function youtube($atts, $content = null) {
	extract(shortcode_atts(array('id' => 'QILiHiTD3uc'), $atts));
	return '<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class="embed-container"><iframe src="//www.youtube.com/embed/' . $id . '" frameborder="0" allowfullscreen></iframe></div>';
}
add_shortcode('youtube', 'youtube');

// Vimeo Embed
function vimeo($atts, $content = null) {
	extract(shortcode_atts(array('id' => '66140585'), $atts));
	return '<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class="embed-container"><iframe src="//player.vimeo.com/video/' . $id .'" frameborder="0" allowfullscreen></iframe></div>';
}
add_shortcode('vimeo', 'vimeo');

// Current Date
function current_date($atts, $content = null) {
   extract(shortcode_atts(array(), $atts));
   return date('Y');
}
add_shortcode('current_date', 'current_date');

// Blog Categories
function blog_categories($atts, $content = null) {
    extract(shortcode_atts(array(), $atts));
    return get_template_part('include/blog-categories');
}
add_shortcode('blog_categories', 'blog_categories');
